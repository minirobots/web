# Minirobots Web


## Docker build

```sh
docker build -t minirobots/web --build-arg MINIROBOTS_GOOGLE_CLIENT_ID --build-arg MINIROBOTS_GOOGLE_SECRET .
```

## Docker run

```sh
docker run --name minirobots-web -d --env MINIROBOTS_GOOGLE_CLIENT_ID --env MINIROBOTS_GOOGLE_SECRET -p 8000:8000  minirobots/web
```
