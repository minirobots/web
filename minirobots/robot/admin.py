from django.contrib import admin
from .models import Turtle, TurtleEvent, TurtleFirmware


@admin.register(Turtle)
class TurtleAdmin(admin.ModelAdmin):
    list_display = ('code', 'comments')


@admin.register(TurtleEvent)
class TurtleEventAdmin(admin.ModelAdmin):
    list_display = ('turtle', 'ip', 'ssid', 'firmware_version', 'created')


@admin.register(TurtleFirmware)
class TurtleFirmwareAdmin(admin.ModelAdmin):
    list_display = ('version', 'changelog')
