from django.db import models


class Turtle(models.Model):
    mac = models.CharField(max_length=17, null=False, unique=True)
    code = models.CharField(max_length=6, null=False, unique=True)
    comments = models.TextField(null=True)

    pen_up = models.IntegerField(null=False, default=-1)
    pen_down = models.IntegerField(null=False, default=-1)

    steps_per_degree = models.FloatField(null=False, default=-1)
    steps_per_mm = models.FloatField(null=False, default=-1)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.code


class TurtleEvent(models.Model):
    ip = models.GenericIPAddressField(null=False)
    ssid = models.CharField(max_length=100, null=True)
    firmware_version = models.CharField(max_length=20, null=False)

    created = models.DateTimeField(auto_now_add=True)

    turtle = models.ForeignKey(Turtle, on_delete=models.CASCADE)

    def __str__(self):
        return f'{{TurtleEvent={self.turtle},{self.ip},{self.created}}}'


class TurtleFirmware(models.Model):
    version = models.CharField(max_length=20, null=False, unique=True)
    changelog = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.version