# Generated by Django 3.1.3 on 2020-11-20 10:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Turtle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mac', models.CharField(max_length=17, unique=True)),
                ('code', models.CharField(max_length=6, unique=True)),
                ('pen_up', models.IntegerField(default=-1)),
                ('pen_down', models.IntegerField(default=-1)),
                ('steps_per_degree', models.FloatField(default=-1)),
                ('steps_per_mm', models.FloatField(default=-1)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='TurtleFirmware',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('version', models.CharField(max_length=20, unique=True)),
                ('changelog', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='TurtleEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip', models.GenericIPAddressField()),
                ('ssid', models.CharField(max_length=100, null=True)),
                ('firmware_version', models.CharField(max_length=20)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('turtle', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='robot.turtle')),
            ],
        ),
    ]
