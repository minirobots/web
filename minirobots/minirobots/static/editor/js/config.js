// vim: expandtab ts=2 sw=2 ai

/**
 * Minirobots - Turtle Web Editor
 * https://minirobots.com.ar
 *
 * Author: Leo Vidarte <lvidarte@gmail.com>
 *
 * This is free software,
 * you can redistribute it and/or modify it
 * under the terms of the GPL version 3
 * as published by the Free Software Foundation.
 *
 */

'use strict';

var config = {
    'api_protocol': 'http',
    'api_minirobots': 'api.minirobots.com.ar',
    'ip': '192.168.1.101',
};
