from django.db import models

from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail import blocks
from wagtail.images.blocks import ImageChooserBlock

from blog.models import BlogPost
from tutorials.models import TutorialPost


class BannerBlock(blocks.StructBlock):
    title = blocks.CharBlock(max_length=100, required=False, help='Add your title')
    body = blocks.CharBlock(max_length=100, required=False)
    image = ImageChooserBlock(required=True)
    url = blocks.URLBlock(required=False)

    class Meta:
        template = 'home/banner_block.html'
        icon = 'edit'
        label = 'Content'


class HomePage(Page):
    body = RichTextField(blank=True)

    banners = StreamField(
        [
            ('banner', BannerBlock()),
        ],
        null=True,
        blank=True,
        use_json_field=True,
    )

    show_banners = models.BooleanField(help_text="Show banners section")
    show_banners_controls = models.BooleanField(help_text="Show next/prev controls arrows")
    show_banners_indicators = models.BooleanField(help_text="Show banners page indicators")
    show_banners_fade_transition = models.BooleanField(help_text="Animate slides with a fade transition instead of a slide")

    #section = StreamField(
    #    [
    #        ('section', SectionBlock()),
    #    ],
    #    null=True,
    #    blank=True,
    #)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname='full'),
        FieldPanel('banners'),
        FieldPanel('show_banners'),
        FieldPanel('show_banners_controls'),
        FieldPanel('show_banners_indicators'),
        FieldPanel('show_banners_fade_transition'),
        #StreamFieldPanel('section'),
    ]

    def get_context(self, request):
        # Filter by tag
        ##tag = request.GET.get('tag')
        blog_posts = BlogPost.objects.live().order_by('-first_published_at')
        tutorial_posts = TutorialPost.objects.live().order_by('-first_published_at')

        # Update template context
        context = super().get_context(request)
        context['blog_posts'] = blog_posts
        context['tutorial_posts'] = tutorial_posts
        return context
