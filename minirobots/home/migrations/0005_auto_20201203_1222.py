# Generated by Django 3.1.3 on 2020-12-03 12:22

from django.db import migrations
import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_auto_20201203_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='section',
            field=wagtail.fields.StreamField([('section', wagtail.blocks.StructBlock([('title', wagtail.blocks.CharBlock(help='Add your title', required=True)), ('body', wagtail.blocks.TextBlock(required=True)), ('image', wagtail.images.blocks.ImageChooserBlock(required=True))]))], null=True),
        ),
    ]
