# Generated by Django 4.2.5 on 2023-09-08 18:48

from django.db import migrations
import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0011_homepage_show_banners_fade_transition'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='banners',
            field=wagtail.fields.StreamField([('banner', wagtail.blocks.StructBlock([('title', wagtail.blocks.CharBlock(help='Add your title', max_length=100, required=False)), ('body', wagtail.blocks.CharBlock(max_length=100, required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=True)), ('url', wagtail.blocks.URLBlock(required=False))]))], blank=True, null=True, use_json_field=True),
        ),
    ]
