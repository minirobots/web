Django>=4.2,<4.3
wagtail>=5.1,<5.2
django-allauth==0.54.0
django-shortuuidfield==0.1.3