from django.db import models
from django.contrib.auth.models import User

from wagtail.models import Page
from wagtail.admin.panels import FieldPanel

from shortuuidfield import ShortUUIDField


class Program(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(max_length=200)
    code = models.TextField(blank=False, null=False)
    public = models.BooleanField(default=True)
    image = models.ImageField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)


class EditorPage(Page):

    def get_context(self, request):
        context = super(EditorPage, self).get_context(request)
        if request.user.is_authenticated:
            context['programs'] = Program.objects.all().filter(author=request.user).order_by('-updated_at')
        if 'uuid' in request.GET:
            if request.user.is_authenticated:
                try:
                    context['program'] = Program.objects.get(author=request.user, uuid=request.GET['uuid']) 
                except:
                    pass
            else:
                try:
                    context['program'] = Program.objects.get(uuid=request.GET['uuid'], public=True)
                except:
                    pass
        return context

    #title = models.CharField(max_length=100, blank=True)

    #content_panels = Page.content_panels + [
    #    FieldPanel('title'),
    #]
