import json
import base64

from django.shortcuts import get_object_or_404
from django.views import generic
from django.http import JsonResponse
from django.core import serializers
from django.core.files.base import ContentFile

from .models import Program
from .forms import ProgramForm


class ProgramListView(generic.ListView):
    model = Program

def postProgram(request):
    # request should be ajax and method should be POST.
    if request.is_ajax and request.method == "POST":
        # get the form data
        data = json.loads(request.body)
        # add current user
        data['author'] = request.user
        if data['uuid']:
            program = get_object_or_404(Program, uuid=data['uuid'])
            form = ProgramForm(data, instance=program)
        else:
            form = ProgramForm(data)
        # save the data and after fetch the object in instance
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()

            # Save image
            image_data = data['image']
            format_, imgstr = image_data.split(';base64,')
            ext = format_.split('/')[-1]
            image = ContentFile(base64.b64decode(imgstr))  
            file_name = instance.uuid + '.' + ext
            instance.image.save(file_name, image, save=True)

            return JsonResponse(
                {
                    "uuid": instance.uuid,
                    "name": instance.name,
                    "image": instance.image.url,
                    "public": instance.public,
                    "updated_at": instance.updated_at,
                },
                status=200,
            )
        else:
            # some form errors occured.
            return JsonResponse({"error": form.errors}, status=400)

    # some error occured
    return JsonResponse({"error": ""}, status=400)
