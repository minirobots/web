from .models import Program
from django import forms


class ProgramForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProgramForm, self).__init__(*args, **kwargs)
    
    class Meta:
        model = Program
        fields = ("__all__")