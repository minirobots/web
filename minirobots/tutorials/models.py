from django.db import models

from wagtail.models import Page
from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.search import index


class TutorialsIndex(Page):
    '''Tutorials index page.'''

    template = 'tutorials/tutorials_index.html'

    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context['posts'] = TutorialPost.objects.live().public()
        return context


class TutorialPost(Page):
    '''Tutorial post page.'''

    template = 'tutorials/tutorial_post.html'

    date = models.DateField('Tutorial date')
    intro = models.CharField(max_length=250)
    image = models.ForeignKey(
        'wagtailimages.Image',
        blank=False,
        null=True,
        related_name='+',
        on_delete=models.SET_NULL,
    )
    body = RichTextField()

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        FieldPanel('image'),
        FieldPanel('body', classname='full'),
    ]
