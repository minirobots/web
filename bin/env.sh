#!/bin/bash

function create_env {
  if ! [[ -d "env" ]]
  then
      python3 -m venv env
      source env/bin/activate
      pip install --upgrade pip
      pip install -r minirobots/requirements.txt
      deactivate
  fi
}

function run_command {
  local readonly command=$1
  
  create_env
  source env/bin/activate

  cd minirobots
  python manage.py $command
  cd -

  deactivate
}

function shell {
  run_command shell
}

function runserver {
  run_command runserver
}

case $1 in
  create) create_env ;;
  shell) shell ;;
  run|runserver) runserver ;;
  *) run_command $1 ;;
esac
